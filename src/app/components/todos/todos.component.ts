import { Component, OnInit } from '@angular/core';
import {TodoService} from '../../services/todo.service';
import { ToDoItem } from '../../models/ToDoItem';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos: ToDoItem[] = []; // models/ToDoItem

  constructor(private ts: TodoService) { }

  ngOnInit(): void {
    this.ts.getTodos().pipe(
      map((response) => response)
      ).subscribe((t: ToDoItem[]) => {
        t?.forEach(element => {
          this.todos.push(element);
        });
    });

    console.log(this.todos);
  }

  toggleTodoItem(todo: ToDoItem): void{
    const pos = this.todos.map(e => e.title ).indexOf(todo.title);
    // Toggle in UI
    todo.completed = !todo.completed;
    // Toggle on Server
    // this.ts.toggleCompleted(todo, pos).subscribe(td =>
    //   console.log(td));
    this.ts.addTodos(this.todos).subscribe(); // Update new Array of Todos in Server
  }

  deleteTodoItem(todo: ToDoItem): void {
    console.log('delete me! todos.component.ts'); 
    const pos = this.todos.map(function(e) { return e.title; }).indexOf(todo.title);
    console.log(pos);
    this.todos = this.todos.filter(t => t.id !== todo.id); // delete from UI
    // this.ts.delTodo(todo, pos).subscribe(); //delete from Server
    this.ts.addTodos(this.todos).subscribe(); // Update new Array of Todos in Server
  }



  deleteTodoItemsss(todo: ToDoItem): void {
    console.log('delete me! todos.component.ts');
    this.todos = this.todos.filter(t => t.id !== todo.id); // delete from UI
    let pos = -1;
    this.todos.forEach(element => {
      if (element.title === todo.title){
        console.log(element.title + ' and ' + todo.title);
        pos = this.todos.indexOf(element);
      }
    });
    console.log(pos);
    this.ts.delTodo(todo, pos).subscribe(); // delete from Server
  }

  addTodoItem(todo: ToDoItem): void { // subscribe to the obversable
    // console.log(this.todos);
    this.todos.push(todo);
    this.ts.addTodos(this.todos).subscribe();
  }

}
