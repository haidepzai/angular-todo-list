import { ToDoItem } from 'src/app/models/ToDoItem';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';


@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {
  @Output() addTd: EventEmitter<any> = new EventEmitter();

  title: string;

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    const toDo: ToDoItem = {
      id: new Date().getUTCMilliseconds(),
      title: this.title, // asign title of toDo
      completed: false // by default
    };

    console.log(toDo);
    // this.todoService.addTodo(toDo);
    this.addTd.emit(toDo);
  }

}
