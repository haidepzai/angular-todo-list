import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TodoService } from '../../services/todo.service';

import { ToDoItem } from 'src/app/models/ToDoItem';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
  @Input() toDoItemComponent: ToDoItem; // Model
  @Output() delTodoItem: EventEmitter<ToDoItem> = new EventEmitter();
  @Output() toggleTodoItem: EventEmitter<ToDoItem> = new EventEmitter();

  constructor(private tds: TodoService) { }

  ngOnInit(): void {
  }

  // Set Dynamic Classes
  setClasses(): object {
    // mehrere Klassen class="item-outline is-complete" wobei is-complete je nachdem ob Häckchen gesetzt ist oder nicht
    const classes = {
      'item-outline': true,
      'is-complete': this.toDoItemComponent.completed
    };
    return classes;
  }

  onToggle(toDoItemComponent): void {
    console.log('toggle');
    this.toggleTodoItem.emit(toDoItemComponent);
  }

  onDelete(toDoItemComponent): void {
    console.log('delete todo-item.component.ts');
    console.log(toDoItemComponent);
    this.delTodoItem.emit(toDoItemComponent); // 2. ruft delTodoItem bei todos.component.html auf
  }

}
