import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodosComponent} from './components/todos/todos.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';

const routes: Routes = [
  { path: '', component: TodoItemComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
