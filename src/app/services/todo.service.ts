import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ToDoItem } from '../models/ToDoItem';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  databaseUrl = 'https://todo-app-6079a.firebaseio.com/';
  databaseEndpoint = 'todos.json';
  todosLimit = '?_limit=3';
  todos: ToDoItem[] = [];

  constructor(private http: HttpClient) { }

  addTodo(todo: ToDoItem){
    this.todos.push(todo);
  }

  // Get Todos
  getTodos(): Observable<ToDoItem[]> {
    return this.http.get<ToDoItem[]>(`${this.databaseUrl}${this.databaseEndpoint}`);
  }

  // Delete Todo
  delTodo(todoItem: ToDoItem, index: number): Observable<ToDoItem> {
    const url = `${this.databaseUrl}${this.databaseEndpoint}/${index}`;
    console.log(url);
    return this.http.delete<ToDoItem>(url, httpOptions); // delete from Server
  }

  // Add Todo
  addTodos(todos: ToDoItem[]): Observable<ToDoItem> {
    const body = JSON.stringify(todos);
    return this.http.put<ToDoItem>(this.databaseUrl + this.databaseEndpoint, body, httpOptions);
  }

  // Toggle Completed
  toggleCompleted(todoItem: ToDoItem, index: number): Observable<any> {
    const url = `${this.databaseUrl}${this.databaseEndpoint}/${index}`; // put Request (url/id from item)
    return this.http.put(url, todoItem, httpOptions);
  }
}
